package com.synopsys.anaplanintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AnaplanIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnaplanIntegrationApplication.class, args);
	}

}
