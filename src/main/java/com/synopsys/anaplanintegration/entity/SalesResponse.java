package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesResponse {
	
	
	private Attributes attributes;
	
	@JsonProperty("Amount")
	private String amount;
	
	@JsonProperty("Business_Type__c")
	private String business_Type_c;
	
	@JsonProperty("Name")
	private String Name;
	
	@JsonProperty("Opportunity_ID_18__c")
	private String opportunityId;
	
	@JsonProperty("Plane_Sourced__c")
	private String planeSourced;
	
	@JsonProperty("Sales_Qualified_Date__c")
	private String salesQualifiedDate;
	
	@JsonProperty("Sales_Status__c")
	private String salesStatus;
	
	@JsonProperty("Sales_Qualified_Territory__c")
	private String salesQualifiedTerritory;
	
	@JsonProperty("SDR_Sourced__c")
	private String sdrSourced;
	
	@JsonProperty("StageName")
	private String stageName;
	
	@JsonProperty("Account")
	private SalesAccount salesAccount;
	
	@JsonProperty("RecordType")
	private SalesAccount recordType;
	
	
	

	public SalesResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SalesResponse(Attributes attributes, String amount, String business_Type_c, String name,
			String opportunityId, String planeSourced, String salesQualifiedDate, String salesStatus,
			String salesQualifiedTerritory, String sdrSourced, String stageName, SalesAccount salesAccount,
			SalesAccount recordType) {
		super();
		this.attributes = attributes;
		this.amount = amount;
		this.business_Type_c = business_Type_c;
		Name = name;
		this.opportunityId = opportunityId;
		this.planeSourced = planeSourced;
		this.salesQualifiedDate = salesQualifiedDate;
		this.salesStatus = salesStatus;
		this.salesQualifiedTerritory = salesQualifiedTerritory;
		this.sdrSourced = sdrSourced;
		this.stageName = stageName;
		this.salesAccount = salesAccount;
		this.recordType = recordType;
	}

	public Attributes getAttributes() {
		return attributes;
	}

	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBusiness_Type_c() {
		return business_Type_c;
	}

	public void setBusiness_Type_c(String business_Type_c) {
		this.business_Type_c = business_Type_c;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(String opportunityId) {
		this.opportunityId = opportunityId;
	}

	public String getPlaneSourced() {
		return planeSourced;
	}

	public void setPlaneSourced(String planeSourced) {
		this.planeSourced = planeSourced;
	}

	public String getSalesQualifiedDate() {
		return salesQualifiedDate;
	}

	public void setSalesQualifiedDate(String salesQualifiedDate) {
		this.salesQualifiedDate = salesQualifiedDate;
	}

	public String getSalesStatus() {
		return salesStatus;
	}

	public void setSalesStatus(String salesStatus) {
		this.salesStatus = salesStatus;
	}

	public String getSalesQualifiedTerritory() {
		return salesQualifiedTerritory;
	}

	public void setSalesQualifiedTerritory(String salesQualifiedTerritory) {
		this.salesQualifiedTerritory = salesQualifiedTerritory;
	}

	public String getSdrSourced() {
		return sdrSourced;
	}

	public void setSdrSourced(String sdrSourced) {
		this.sdrSourced = sdrSourced;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public SalesAccount getSalesAccount() {
		return salesAccount;
	}

	public void setSalesAccount(SalesAccount salesAccount) {
		this.salesAccount = salesAccount;
	}

	public SalesAccount getRecordType() {
		return recordType;
	}

	public void setRecordType(SalesAccount recordType) {
		this.recordType = recordType;
	}
	
	
	
	

}
