package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SfdcAuthenticationResponse {

	private String access_token;
	private String instance_url;
	private String id;
	private String token_type;
	private String issued_at;
	private String signature;
	
	
}
