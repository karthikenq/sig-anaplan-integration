package com.synopsys.anaplanintegration.entity;

public class Accounts {
	
	private String accountId;
	
	private String accountName;
	
	private String opportunityId;
	
	private String opportunityName;
	
	private String sapSiteLogo;
	
	private String globalAccount;
	
	private String opportunityRecordType;
	
	private String businessType;
	
	private String opportunityOwner;
	
	private String userId;
	
	private String salesTeam;
	
	private String salesGeo;
	
	private String salesRegion;
	
	private String salesTerritory;
	
	private String salesRole;
	
	private String closeDate;
	
	private String Stage;


}
