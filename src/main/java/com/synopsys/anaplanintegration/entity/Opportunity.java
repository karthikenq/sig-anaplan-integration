package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Opportunity {

	@JsonProperty("attributes")
	private Attributes attributes;
	
	@JsonProperty("X1st_Services_Booking__c")
	private String servicesBoking;
	
	@JsonProperty("X1st_Year_New_Growth_Booking__c")
	private String newYearGrowthBooking;
	
	@JsonProperty("Beyond_1st_Services_Booking__c")
	private String beyondServicesBooking;
	
	@JsonProperty("Beyond_1st_Year_New_Growth_Booking__c")
	private String beyondNewYearGrowthBooking;
	
	@JsonProperty("Business_Type__c")
	private String businessType;
	
	@JsonProperty("CloseDate")
	private String closeDate;
	
	@JsonProperty("Expiring_Contract_Duration__c")
	private String expiringContractDuration;
	
	@JsonProperty("Expiring_Contract_TCV__c")
	private String expiringContractTCV;
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Opportunity_ID_18__c")
	private String opportunityId;
	
	@JsonProperty("Renewable_Booking_at_Term__c")
	private String renewableBookingAtTerm;
	
	@JsonProperty("Renewable_Booking_Beyond_Term__c")
	private String renewableBookingBeyondTerm;
	
	@JsonProperty("SAP_Contract_Number__c")
	private String sapContractNumber;
	
	@JsonProperty("SharePoint_deal_ID__c")
	private String sharePointDealId;
	
	@JsonProperty("StageName")
	private String stageName;
	
	@JsonProperty("Account")
	private Account account;
	
	@JsonProperty("RecordType")
	private RecordType recordType;
	
	@JsonProperty("Owner")
	private Owner owner;
	
	@JsonProperty("CRM_Owner__r")
	private CrmOwner crmOwner;
	

	
}
