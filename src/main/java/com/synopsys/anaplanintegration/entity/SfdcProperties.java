package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SfdcProperties {

	
	private String fileName;
	
    private String processId;
	
	private String soqlQuery;
	
	private String integrationId;	
	
	private String fileId;
	
}
