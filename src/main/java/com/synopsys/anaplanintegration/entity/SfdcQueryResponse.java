package com.synopsys.anaplanintegration.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SfdcQueryResponse {

	
	private int totalSize;
	
	private String done;
	
	private String nextRecordsUrl;
	
	private List<SalesResponse> records;

	
	public SfdcQueryResponse() {
		super();
	}

	public SfdcQueryResponse(int totalSize, String done, String nextRecordsUrl, List<SalesResponse> salesResponse) {
		super();
		this.totalSize = totalSize;
		this.done = done;
		this.nextRecordsUrl = nextRecordsUrl;
		this.records = salesResponse;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	public String getDone() {
		return done;
	}

	public void setDone(String done) {
		this.done = done;
	}

	public String getNextRecordsUrl() {
		return nextRecordsUrl;
	}

	public void setNextRecordsUrl(String nextRecordsUrl) {
		this.nextRecordsUrl = nextRecordsUrl;
	}

	public List<SalesResponse> getRecords() {
		return records;
	}

	public void setRecords(List<SalesResponse> salesResponse) {
		this.records = salesResponse;
	}
	
	
	
	
}
