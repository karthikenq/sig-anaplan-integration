package com.synopsys.anaplanintegration.entity;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesQueryResponse {

	@JsonProperty("totalSize")
	private int totalSize;
	
	@JsonProperty("done")
	private String done;
	
	@JsonProperty("nextRecordsUrl")
	private String nextRecordsUrl;
	
	@JsonProperty("records")
	private List<SalesQualifyingLead> records;
	

}
