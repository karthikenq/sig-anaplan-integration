package com.synopsys.anaplanintegration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner {

	@JsonProperty("attributes")
	private Attributes attributes;
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Sales_Geo__c")
	private String salesGeo;
	
	@JsonProperty("Sales_Region__c")
	private String salesRegion;
	
	@JsonProperty("Sales_Role__c")
	private String salesRole;
	
	@JsonProperty("Sales_Team__c")
	private String salesTeam;
	
	@JsonProperty("Sales_Territory__c")
	private String salesTerritory;
	
	@JsonProperty("USER_ID_18__c")
	private String userId;
	
	
	
}

