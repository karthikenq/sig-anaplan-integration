package com.synopsys.anaplanintegration.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.synopsys.anaplanintegration.service.SfdctoAnaplanService;

@Component
public class SfdcScheduler {
	
	@Value("${spring.plan.dailyJob}")
	private String dailyJobexportId;
	
	
	@Value("${spring.plan.adhocJob}")
	private String adhocJobexportId;
	
	@Autowired
	SfdctoAnaplanService  sfdctoAnaplanService;
	
	private static final Logger logger = LogManager.getLogger(SfdcScheduler.class);
	
//	@Scheduled(fixedRate = 60000)
	public void dailyJob()
	{
		logger.info("Daily Job Scheduler Started");
		sfdctoAnaplanService.exportFromSfdctoAnaplan(dailyJobexportId);
		logger.info("Daily Job Scheduler Completed");
	}
	
	//@Scheduled(fixedRate = 60000)
	public void adhocJob()
	{
		logger.info("Daily Job Scheduler Started");
		sfdctoAnaplanService.exportFromSfdctoAnaplan(adhocJobexportId);
	}
	

	

}
