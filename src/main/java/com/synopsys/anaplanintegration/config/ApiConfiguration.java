package com.synopsys.anaplanintegration.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring")
@SuppressWarnings("unused")
public class ApiConfiguration {

	@Profile("dev")
	@Bean
	public String devConfiguration() {
		
		return "Setting up Application for DEV environmrnt";
	}
	
	@Profile("prod")
	@Bean
	public String prodConfiguration() {
		
		return "Setting up Application for PROD environmrnt";
    
	}
	
	@Profile("stg")
	@Bean
	public String stgConfiguration() {
		
	//	System.out.println("Setting up Application for STG environmrnt");
		return "Setting up Application for STG environmrnt";
	
	}	
	
	
}
