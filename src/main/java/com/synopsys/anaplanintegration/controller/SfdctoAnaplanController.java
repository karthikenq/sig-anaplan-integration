package com.synopsys.anaplanintegration.controller;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synopsys.anaplanintegration.service.SfdctoAnaplanService;

@RestController
public class SfdctoAnaplanController {

	private static final Logger logger = LogManager.getLogger(SfdctoAnaplanController.class);

	private SfdctoAnaplanService sfdctoAnaplanService;

	@Autowired
	public SfdctoAnaplanController(SfdctoAnaplanService sfdctoAnaplanService) {

		this.sfdctoAnaplanService = sfdctoAnaplanService;
	}

	@PostMapping("/sync/sfdc-anaplan/{exportId}")
	// @Scheduled(fixedRate = 60000)
	public ResponseEntity<String> exportFromSfdctoAnaplan(@PathVariable String exportId) throws IOException {

		sfdctoAnaplanService.exportFromSfdctoAnaplan(exportId);

		return new ResponseEntity<>("Success", HttpStatus.CREATED);

	}

}
