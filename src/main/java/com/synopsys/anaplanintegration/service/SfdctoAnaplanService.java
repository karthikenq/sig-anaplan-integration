package com.synopsys.anaplanintegration.service;

public interface SfdctoAnaplanService {

	String exportFromSfdctoAnaplan(String exportId);
	
	String uploadDate();
	
}
