package com.synopsys.anaplanintegration.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.synopsys.anaplanintegration.service.AnaplanOperationsService;
import com.synopsys.anaplanintegration.util.CustomFileToJson;

@Service
public class AnaplanOperationsServiceImpl implements AnaplanOperationsService{

	
	@Value("${spring.plan.username}")
	private String username;
	
	@Value("${spring.plan.password}")
	private String password;
	
	@Value("${spring.plan.workspaceId}")
	private String workspaceId;;
	
	@Value("${spring.plan.modelId}")
	private String modelId;
	
	
	private static final String baseUrl="https://api.anaplan.com/1/3/workspaces/";
	
	private static final Logger LOGGER = LogManager.getLogger(AnaplanOperationsServiceImpl.class);
	
	private CustomFileToJson jsonUtil=new CustomFileToJson();
	
	

	@Override
	public String runExportAction(String exportId) {
		
		
		final String exportUrl= baseUrl+workspaceId+"/models/"+modelId+"/exports/"+exportId+"/tasks";
	    final String chunkUrl = baseUrl+workspaceId+"/models/"+modelId+"/files/"+exportId+"/chunks/0";
	    
	    
	    RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders fileHeaders = new HttpHeaders();
		HttpHeaders exportHeaders=new HttpHeaders();
		exportHeaders.setBasicAuth(username, password);
		fileHeaders.setBasicAuth(username, password);
		
		exportHeaders.add("Content-Type", "application/json");
		
        HttpEntity<String> exportRequest = new HttpEntity<>("{\"localeName\": \"en_US\"}",exportHeaders);
		
		ResponseEntity<?> exportResponse = restTemplate.exchange(exportUrl, HttpMethod.POST, exportRequest,String.class);
		
	    String exportData=(String) exportResponse.getBody();
		
		LOGGER.info("Export has Run successfully");
		
		
		fileHeaders.add("Accept", "application/octet-stream");
		
        HttpEntity<String> fileRequest = new HttpEntity<>(fileHeaders);
		
		ResponseEntity<?> fileResponse = restTemplate.exchange(chunkUrl, HttpMethod.GET, fileRequest,String.class);
		
		String fileData=(String) fileResponse.getBody();

		String responseBody = jsonUtil.convertToJson(fileData);
		
		LOGGER.info("File Download successfully");
	   
	    
		return responseBody;
	}

	@Override
	public String runImportAction(Integer importId) {

		
		LOGGER.info("Import Action Invoked");
		return null;
	}

	@Override
	public String runProcess(String processId) {
		
		final String processUrl=baseUrl+workspaceId+"/models/"+modelId+"/processes/"+processId+"/tasks";
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders processHeaders=new HttpHeaders();
		processHeaders.setBasicAuth(username, password);
		processHeaders.add("Content-Type", "application/json");
		
		HttpEntity<String> processRequest = new HttpEntity<>("{\"localeName\": \"en_US\"}",processHeaders);
		
		ResponseEntity<?> processResponse = restTemplate.exchange(processUrl, HttpMethod.POST, processRequest,String.class);
		
		String processResult=(String) processResponse.getBody();
		
		return processResult;
		
	}

	@Override
	public String uploadFile(String fileId,String fileStream) {
		
		final String fileUploadUrl=baseUrl+workspaceId+"/models/"+modelId+"/files/"+fileId;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders fileHeaders=new HttpHeaders();
		fileHeaders.setBasicAuth(username, password);
		fileHeaders.add("Content-Type", "application/octet-stream");
		
		HttpEntity<String> fileRequest = new HttpEntity<>(fileStream,fileHeaders);
		
		ResponseEntity<?> fileResponse = restTemplate.exchange(fileUploadUrl, HttpMethod.PUT, fileRequest,String.class);
		
		String fileResult=(String) fileResponse.getBody();
		
		LOGGER.info(fileResponse.getStatusCodeValue());
		
		return fileResult;
	}

}
