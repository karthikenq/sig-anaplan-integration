package com.synopsys.anaplanintegration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synopsys.anaplanintegration.entity.OpportunityQueryResponse;
import com.synopsys.anaplanintegration.entity.QueryRequest;
import com.synopsys.anaplanintegration.entity.SalesQueryResponse;
import com.synopsys.anaplanintegration.entity.SfdcProperties;
import com.synopsys.anaplanintegration.exception.CustomException;
import com.synopsys.anaplanintegration.service.AnaplanOperationsService;
import com.synopsys.anaplanintegration.service.SfdcOperationsService;
import com.synopsys.anaplanintegration.service.SfdctoAnaplanService;
import com.synopsys.anaplanintegration.util.CustomJsonToFile;

@Service
public class SfdctoAnaplanServiceImpl implements SfdctoAnaplanService {

	private AnaplanOperationsService anaplanOperationService;

	private SfdcOperationsService sfdcOperationService;

	ObjectMapper mapper = new ObjectMapper();

	private CustomJsonToFile customJsonTofile = new CustomJsonToFile();

	private static final Logger logger = LogManager.getLogger(SfdctoAnaplanServiceImpl.class);

	@Autowired
	public SfdctoAnaplanServiceImpl(AnaplanOperationsService anaplanOperationService,
			SfdcOperationsService sfdcOperationService) {

		this.anaplanOperationService = anaplanOperationService;
		this.sfdcOperationService = sfdcOperationService;

	}

	@Override
	public String exportFromSfdctoAnaplan(String exportId) {

		String soqlQueryResponse,result;
		String nextRecordUrl = "/services/data/v42.0/query";

		QueryRequest queryRequest = new QueryRequest();

		SalesQueryResponse salesQueryResponse;
		OpportunityQueryResponse opportunityQueryResponse;

		StringBuilder salesFileResponse = new StringBuilder();
		StringBuilder oppFileResponse = new StringBuilder();

		salesFileResponse
				.append("Opportunity ID (18)	Plane Sourced	Sales Qualified Territory	Opportunity Record Type	SDR Sourced	Sales Qualified Date	Sales Qualified Status	Business Type	Account Name	"
						+ "Opportunity Name	Stage	Amount	Last Updated Date");

		oppFileResponse
				.append("Account ID (18)	Account Name	Opportunity ID (18)	Opportunity Name	SAP Site Logo	Global Account	Opportunity Record Type	Business Type	Opportunity Owner	"
						+ "USER ID (18)	Sales Team	Sales Geo	Sales Region	Sales Territory	Sales Role	Close Date	Stage	"
						+ "1st Year New/Growth Booking	1st  Services Booking	Beyond 1st Year New/Growth Booking 	"
						+ "Beyond 1st Services Booking	Renewable Booking at Term	"
						+ "Renewable Booking Beyond Term	Expiring Contract TCV	Expiring Contract Duration	CRM Owner	SAP Contract Number	SharePoint deal ID	Last Updated Date");

		try {

			result = anaplanOperationService.runExportAction(exportId);

			List<SfdcProperties> sfdcProperties = mapper.readValue(result, new TypeReference<List<SfdcProperties>>() {
			});

			logger.info(result);

			for (int i = 0; i < sfdcProperties.size(); i++) {

				logger.info(sfdcProperties.get(i).getFileId());

				if (sfdcProperties.get(i).getFileName().equals("Opportunities")) {

					logger.info("File Uploading for Opportunities");

					do {

						queryRequest.setNextrecordUrl(nextRecordUrl);

						queryRequest.setSoqlQuery(sfdcProperties.get(i).getSoqlQuery());

						soqlQueryResponse = sfdcOperationService.getQueryResult(queryRequest);

						opportunityQueryResponse = mapper.readValue(soqlQueryResponse, OpportunityQueryResponse.class);

						oppFileResponse.append(
								customJsonTofile.convertOpportunityToFile(opportunityQueryResponse.getRecords()));

						nextRecordUrl = opportunityQueryResponse.getNextRecordsUrl();

					} while (opportunityQueryResponse.getNextRecordsUrl() != null);

					anaplanOperationService.uploadFile(sfdcProperties.get(i).getFileId(), oppFileResponse.toString());

					logger.info("Opportunities File Upload Successffully");

					anaplanOperationService.runProcess(sfdcProperties.get(i).getProcessId());

					logger.info("Process Run Successffully");
					
				

				} else if (sfdcProperties.get(i).getFileName().equals("Sales_Qualifying_Lead")) {

					logger.info("File Uploading for Sales Qulaifying Lead");
					
					nextRecordUrl = "/services/data/v42.0/query";	

					do {

						queryRequest.setNextrecordUrl(nextRecordUrl);

						queryRequest.setSoqlQuery(sfdcProperties.get(i).getSoqlQuery());

						soqlQueryResponse = sfdcOperationService.getQueryResult(queryRequest);

						salesQueryResponse = mapper.readValue(soqlQueryResponse, SalesQueryResponse.class);

						salesFileResponse.append(customJsonTofile.convertSalesToFile(salesQueryResponse.getRecords()));

						nextRecordUrl = salesQueryResponse.getNextRecordsUrl();

					} while (salesQueryResponse.getNextRecordsUrl() != null);

					anaplanOperationService.uploadFile(sfdcProperties.get(i).getFileId(), salesFileResponse.toString());

					logger.info("Sales Qualifying Lead File Upload Successffully");

					anaplanOperationService.runProcess(sfdcProperties.get(i).getProcessId());

					logger.info("Process Run Successffully");
				}

				else {
					logger.info("Wrong File Name");
				}
			}
		} catch (Exception ex) {

			throw new CustomException(ex.getMessage());

		}

		return null;
	}

	@Override
	public String uploadDate() {
		
		return null;
	}

}
