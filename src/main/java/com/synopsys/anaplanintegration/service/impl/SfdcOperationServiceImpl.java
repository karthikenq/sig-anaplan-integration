package com.synopsys.anaplanintegration.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.synopsys.anaplanintegration.entity.QueryRequest;
import com.synopsys.anaplanintegration.entity.SfdcAuthenticationResponse;
import com.synopsys.anaplanintegration.service.SfdcOperationsService;

@Service
public class SfdcOperationServiceImpl implements SfdcOperationsService {

	
	private static final Logger LOGGER = LogManager.getLogger(SfdcOperationServiceImpl.class);
	
	@Value("${spring.sfdc.username}")
	private String username;
	
	@Value("${spring.sfdc.password}")
	private String password;
	
	@Value("${spring.sfdc.clientId}")
	private String clientId;
	
	@Value("${spring.sfdc.clientSecret}")
	private String clientSecret;
	
	@Value("${spring.sfdc.authenticationUrl}")
	private String authenticationUrl;
	
	@Value("${spring.sfdc.baseUrl}")
	private String baseUrl;
	
	
	@Override
	public String getQueryResult(QueryRequest soqlQuery) {

		
		   final String queryUrl=baseUrl+soqlQuery.getNextrecordUrl()+"?q="+soqlQuery.getSoqlQuery();
		   RestTemplate restTemplate = new RestTemplate();
		   HttpHeaders authenticationHeaders = new HttpHeaders();
		   authenticationHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		 
		   MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>(); 
		   body.add("username", username);
		   body.add("password", password);
		   body.add("grant_type", "password");
		   body.add("client_id", clientId);
		   body.add("client_secret", clientSecret);
		   
		   HttpEntity<?> authenticationRequest = new HttpEntity<>(body,authenticationHeaders);
		   
		   SfdcAuthenticationResponse authenticationResponse = restTemplate.postForObject(authenticationUrl,authenticationRequest,SfdcAuthenticationResponse.class);
		   
		   String accessToken=authenticationResponse.getAccess_token();
		   
		   LOGGER.info("Authenitication Successfull");
		   
		   HttpHeaders queryHeaders = new HttpHeaders();
		   
		   queryHeaders.add("Authorization", "Bearer "+accessToken);
		   
		   HttpEntity<String> queryRequest = new HttpEntity<>(queryHeaders);
		   
		   LOGGER.info(queryUrl);
		   
		   ResponseEntity<?> queryResponse = restTemplate.exchange(queryUrl, HttpMethod.GET, queryRequest,String.class);
		   
		   String queryData=(String) queryResponse.getBody();
		   
		   LOGGER.info("SOQL Query has retured Results Successfully");
		   
		   
		return queryData;
	}

}
